// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

//test constructor
TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

//test constructor
TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[96], 92);
}                                         // fix test

//test that iterator of empty allocator is not empty
TYPED_TEST(AllocatorFixture2, test4) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    typename allocator_type::iterator b = x.begin();
    typename allocator_type::iterator e = x.end();
    ASSERT_NE(b, e);
}

//test that iterator correctly returns value of sentinel when added
TYPED_TEST(AllocatorFixture2, test5) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    typename allocator_type::iterator b = x.begin();
    typename allocator_type::iterator e = x.end();
    ASSERT_EQ(*b, 92);
}

//test that iterator correctly increments to end
TYPED_TEST(AllocatorFixture2, test6) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    typename allocator_type::iterator b = x.begin();
    typename allocator_type::iterator e = x.end();
    ++b;
    ASSERT_EQ(b, e);
}

//test allocating entire free block
TYPED_TEST(AllocatorFixture2, test7) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    x.allocate(92/sizeof(typename allocator_type::value_type));
    ASSERT_EQ(x[0], -92);
}

//test allocating only part of free block
TYPED_TEST(AllocatorFixture2, test8) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    x.allocate(40/sizeof(typename allocator_type::value_type));
    typename allocator_type::iterator b = x.begin();
    ASSERT_EQ(*b, -40);
    ++b;
    ASSERT_EQ(*b, 44);
}

//test allocating part of free block, but not enough is left for more data
TYPED_TEST(AllocatorFixture2, test9) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    x.allocate(88/sizeof(typename allocator_type::value_type));
    ASSERT_EQ(x[0], -92);
}

//test allocating all of block, then deallocate it
TYPED_TEST(AllocatorFixture2, test10) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer = typename TestFixture::pointer;

    allocator_type x;
    pointer p = x.allocate(92/sizeof(typename allocator_type::value_type));
    x.deallocate(p, NULL);
    ASSERT_EQ(x[0], 92);
}

//test allocating two blocks, then freeing the first
TYPED_TEST(AllocatorFixture2, test11) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer = typename TestFixture::pointer;

    allocator_type x;
    pointer p = x.allocate(32/sizeof(typename allocator_type::value_type));
    x.allocate(32/sizeof(typename allocator_type::value_type));
    x.deallocate(p, NULL);
    ASSERT_EQ(x[0], 32);
}

//test allocating two blocks, then freeing both (should include coalescing on both sides)
TYPED_TEST(AllocatorFixture2, test12) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer = typename TestFixture::pointer;

    allocator_type x;
    pointer p1 = x.allocate(32/sizeof(typename allocator_type::value_type));
    pointer p2 = x.allocate(32/sizeof(typename allocator_type::value_type));
    x.deallocate(p1, NULL);
    x.deallocate(p2, NULL);
    ASSERT_EQ(x[0], 92);
}

//test iterating after allocating and freeing
TYPED_TEST(AllocatorFixture2, test13) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer = typename TestFixture::pointer;

    allocator_type x;
    pointer p = x.allocate(24/sizeof(typename allocator_type::value_type));
    x.allocate(24/sizeof(typename allocator_type::value_type));
    x.deallocate(p, NULL);
    auto b = x.begin();
    auto e = x.end();
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, 24);
    ++b;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, -24);
    ++b;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, 28);
    ++b;
    ASSERT_EQ(b, e);

}






