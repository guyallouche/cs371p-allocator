# CS371p: Object-Oriented Programming Allocator Repo

* Name: Guy Allouche

* EID: ga22287

* GitLab ID: guyallouche

* HackerRank ID: guy_allouche_1

* Git SHA: d723ccc2abbc00d7d8004fc483a069daa3dcb06b

* GitLab Pipelines: https://gitlab.com/guyallouche/cs371p-allocator/-/pipelines

* Estimated completion time: 8

* Actual completion time: 8

* Comments: My estimated time was pretty much right on the money this time!
