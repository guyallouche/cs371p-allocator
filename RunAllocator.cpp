// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>

using namespace std;

#include "Allocator.hpp"


using datatype = double;
const int heapsize = 1000;

void allocator_worker (my_allocator<datatype, heapsize>& heap, vector<int> instructions) {
    //iterate over each instruction and allocate/deallocate memory
    for (int i = 0; i < instructions.size(); i++) {
        int instruction = instructions[i];
        assert(instruction != 0);
        if (instruction > 0) {
            heap.allocate(instruction);
        } else {
            auto b = heap.begin();
            auto e = heap.end();
            while (instruction < 0) {
                if (*b < 0) {
                    instruction++;
                }
                if (instruction < 0) {
                    ++b;
                }
            }
            datatype* p = reinterpret_cast<datatype*>((&*b) + 1);
            heap.deallocate(p, NULL);
        }
    }
}

void allocator_IO (istream& sin, ostream& sout) {
    string s;
    // first line is number of test cases
    getline(sin, s);
    int t = stoi(s);
    assert(t > 0);
    // there is a blank line after t
    getline(sin, s);
    for (int testcase = 0; testcase < t; testcase++) {
        vector<int> instructions;
        getline(sin, s);
        while (s.length() > 0) {
            instructions.push_back(stoi(s));
            if (!getline(sin, s)) {
                break;
            }
        }
        my_allocator<datatype, heapsize> heap;

        //execute instructions on heap
        allocator_worker (heap, instructions);

        //print result
        auto b = heap.begin();
        auto e = heap.end();
        sout << *b;
        ++b;
        while (b != e) {
            sout << " " << *b;
            ++b;
        }
        sout << endl;
    }
}


// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
    allocator_IO(std::cin, std::cout);
    return 0;
}
