// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    /**
    * @param an allocator
    * @param an allocator
    * @return false, always
    */
    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------
    /**
    * @param an allocator
    * @param an allocator
    * @return true, always
    */
    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
        * @param an iterator
        * @param an iterator
        * @return true if left hand and right hand iterators are pointing at same place in memory
        */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        /**
        * @param an iterator
        * @param an iterator
        * @return true if left hand and right hand iterators are pointing at different places in memory
        */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        /**
        * @param a pointer to an int
        * constructs an iterator pointing to that int, which should be the location of a sentinel
        */
        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        /**
        * @return return the value of the sentinel the iterator is pointing at
        */
        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        /**
        * @return this iterator, pointing at the next sentinel (or end, if no more sentinels)
        */
        iterator& operator ++ () {
            int size = abs(*_p);
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) + 2* sizeof(int) + size);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        /**
        * @return a copy of this iterator, and point this one at the next sentinel (or end, if no more sentinels)
        */
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
        * @return this iterator, pointing at the previous sentinel
        */
        iterator& operator -- () {

            int size = abs(*(_p - 1));
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - 2* sizeof(int) - size);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
        * @return a copy of this iterator, and point this one at the previous sentinel (or beginning, if no more sentinels)
        */
        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
        * @param an iterator
        * @param an iterator
        * @return true if left hand and right hand iterators are pointing at same place in memory
        */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        /**
        * @param an iterator
        * @param an iterator
        * @return true if left hand and right hand iterators are not pointing at same place in memory
        */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------
        /**
        * @return return the a constant reference to value of the sentinel the iterator is pointing at
        */
        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        /**
        * @return this iterator, pointing at the next sentinel (or end, if no more sentinels)
        */
        const_iterator& operator ++ () {
            int size = abs(*_p);
            _p = reinterpret_cast<const int*>(reinterpret_cast<const char*>(_p) + 2* sizeof(int) + size);
            return *this;
        }
        // -----------
        // operator ++
        // -----------

        /**
        * @return a copy of this iterator, and point this one at the next sentinel (or end, if no more sentinels)
        */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
        * @return this iterator, pointing at the previous sentinel
        */
        const_iterator& operator -- () {
            int size = abs(*(_p - 1));
            _p = reinterpret_cast<const int*>(reinterpret_cast<const char*>(_p) - 2* sizeof(int) - size);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
        * @return a copy of this iterator, and point this one at the previous sentinel (or beginning, if no more sentinels)
        */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * @return whether this is a valid allocator
     * that is, all its sentinels accurately reflect the state of memory, none of its blocks contain too little space to fit a T
     * and its blocks add up to the total allocated size
     */
    bool valid () const {
        int sum = 0;
        const_iterator b = begin();
        const_iterator e = end();
        while (b != e) {
            //check that block is not too small to fit a T (illegal block)
            const int* first_sentinel = &*b;
            int block_size = abs(*first_sentinel);
            if (block_size < sizeof(value_type)) {
                return false;
            }
            //check that at the end of this block is another sentinel for it (confirms that sentinels are accurate)
            const int* second_sentinel = reinterpret_cast<const int*>(
                                             reinterpret_cast< const char*>(first_sentinel) + block_size + sizeof(int));
            if (*second_sentinel != *first_sentinel) {
                return false;
            }
            sum += block_size + 2 * sizeof(int);
            ++b;
        }
        return sum == N;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(value_type) + 2*sizeof(int)) {
            throw std::bad_alloc();
        }
        (*this)[0] = (N - 2*sizeof(int));
        (*this)[N - sizeof(int)] = (N - 2*sizeof(int));
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param s the number of objects to allocate
     * @return a pointer to the newly allocated block of memory
     */
    pointer allocate (size_type s) {
        if (s < 1 || s*sizeof(T) + (2*sizeof(int)) > N) {
            throw std::bad_alloc();
        }
        iterator b = begin();
        iterator e = end();
        int size_required = s * sizeof(T);
        while (b != e) {
            int size = *b;
            if (size >= size_required ) {
                break;
            } else {
                b++;
            }
        }
        //can't fit anywhere
        if (b == e) {
            throw std::bad_alloc();
        }
        int* sentinel = &*b;
        int block_size = *sentinel;
        if (block_size - size_required < sizeof(T) + 2 * sizeof(int)) {
            //just give them the whole block
            *sentinel = -block_size;
            *reinterpret_cast<int*>(reinterpret_cast<char*>(sentinel) + block_size + sizeof(int)) = -block_size;
        } else {
            //we should only give them part of the block
            *sentinel = -size_required;
            int* second_sentinel = reinterpret_cast<int*>(reinterpret_cast<char*>(sentinel) + size_required + sizeof(int));
            *second_sentinel = -size_required;
            *(second_sentinel+1) = block_size - size_required - 2 * sizeof(int);
            *reinterpret_cast<int*>(reinterpret_cast<char*>(sentinel) + block_size + sizeof(int)) = block_size - size_required - 2 * sizeof(int);
        }
        assert(valid());
        return reinterpret_cast<T*>(sentinel + 1);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     * @param a pointer p, to create the object in
     * @param v a reference to an object to copy
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param a pointer to the first byte of an allocated memory block
     * @param unused
     * has undefined behavior if pointer does not point to first byte of an allocated block
     * accessing data that is not allocated also has undefined behavior
     */
    void deallocate (pointer block, size_type unused) {
        if (reinterpret_cast<char*>(block) < &a[0] || reinterpret_cast<char*>(block) > &a[N]) {
            throw std::invalid_argument("pointer outside allocator's memory");
        }

        //destroy whatever object the user had stored in this block
        destroy(block);

        int* left = reinterpret_cast<int*>(block) - 1;;
        int size = -1*(*left);
        int* right = reinterpret_cast<int*>(reinterpret_cast<char*>(left) + size + sizeof(int));

        if (*left != *right) {
            throw std::invalid_argument("pointer does not point to first byte of allocated block");
        }

        //check sentinel to left to see if we need to coalesce
        if (reinterpret_cast<char*>(left-1) >= &a[0] && *(left-1) > 0) {
            //we need to coalesce left
            left = reinterpret_cast<int*>(reinterpret_cast<char*>(left-1) - *(left-1) - sizeof(int));
            size += *left + 2 * sizeof(int);
        }
        //checksnetinel to right to see if we need to coalesce
        if (reinterpret_cast<char*>(right+1) < &a[N] && *(right+1) > 0) {
            //we need to coalesce right
            right = reinterpret_cast<int*>(reinterpret_cast<char*>(right+1) + *(right+1) + sizeof(int));
            size += *right + 2 * sizeof(int);
        }

        *left = size;
        *right = size;
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * @param byte of allocator to return the int value of
     * @return reference to said int
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @param byte of allocator to return the int value of
     * @return reference to said int
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator to first sentinel of allocator
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator to first sentinel of allocator
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator off the edge of allocator
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator off the edge of allocator
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
